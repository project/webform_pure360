<?php

/**
 * Implementation of _webform_defaults_component().
 */
function _webform_defaults_pure360() {
  return array(
    'name' => '',
    'form_key' => NULL,
    'required' => 1,
    'pid' => 0,
    'weight' => 0,
    'extra' => array(
      'p360_profile_name' => '',
      'p360_list_name' => '',
      'p360_segment_name' => '',
    ),
  );
}

function _webform_edit_pure360($component) {

  $form = array();
  $node = node_load($component['nid']);

  $existing_email_components = array();
  if (!empty($node->webform['components'])) {
    foreach ($node->webform['components'] AS $field) {
      if ($field['type'] == 'email' && $field['required']) {
        $existing_email_components[$field['form_key']] = $field['name'];
      }
    }
  }

  // required properties
  $form['extra']['required_properties'] = array(
    '#type' => 'fieldset',
    '#title' => t('Required properties'),
  );
  $form['extra']['required_properties']['p360_profile_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Profile name'),
    '#default_value' => !empty($component['extra']['required_properties']['p360_profile_name']) ? $component['extra']['required_properties']['p360_profile_name'] : variable_get('webform_pure360_default_profile', ''),
    '#size' => 60,
    '#required' => TRUE,
    '#maxlength' => 255,
    '#weight' => 1,
    '#description' => t('The profile name associated of your Pure360 account.'),
  );
  $form['extra']['required_properties']['p360_list_name'] = array(
    '#type' => 'textfield',
    '#title' => t('List name'),
    '#default_value' => $component['extra']['required_properties']['p360_list_name'],
    '#size' => 60,
    '#required' => TRUE,
    '#maxlength' => 255,
    '#weight' => 2,
    '#description' => t('The list that submission from this from will be appended to.'),
  );
  $form['extra']['required_properties']['p360_email_map'] = array(
    '#type' => 'select',
    '#title' => t('Email'),
    '#required' => TRUE,
    '#weight' => 3,
    '#options' => $existing_email_components,
    '#default_value' => $component['extra']['required_properties']['p360_email_map'],
    '#description' => t('The component which the value will be taken from.'),
  );

  // @todo: instead of rendering the email field ourselves get the user to choose
  // an existing email component to make this consistent with the optional props

  // optional properties
  $form['extra']['optional_properties'] = array(
    '#type' => 'fieldset',
    '#title' => t('Optional properties'),
  );


  $existing_field_options = array();
  // Fetches existing components, checks if any of them are e-mail fields.
  // Let's the user choose which field to use for the newsletter e-mail address.
  if (!empty($node->webform['components'])) {
    foreach ($node->webform['components'] AS $field) {
      if (in_array($field['type'], array('textfield','hidden','email','select'))) {
        $existing_field_options[$field['form_key']] = $field['name'];
      }
    }
  }

  $op_prop = 0;
  $op_params_max = variable_get('webform_pure360_param_no', 5);
  while($op_prop < $op_params_max) {
    $op_prop_name = $component['extra']['optional_properties'][$op_prop]['name'];
    $op_prop_map = $component['extra']['optional_properties'][$op_prop]['map'];

    $op_prop_fieldset = array(
      '#type' => 'fieldset',
      '#title' => t('Optional property !prop_no', array('!prop_no' => $op_prop+1)),
      '#collapsible' => TRUE,
      '#collapsed' => $op_prop > 3 ? TRUE : FALSE,
    );
    $op_prop_fieldset['name'] = array(
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#size' => 60,
      '#maxlength' => 255,
      '#default_value' => $op_prop_name,
      '#description' => t('The Pure360 property name to map the value to.'),
    );
    $op_prop_fieldset['map'] = array(
      '#type' => 'select',
      '#title' => t('Value'),
      '#options' => $existing_field_options,
      '#default_value' => $op_prop_map,
      '#description' => t('The component which the value will be taken from.'),
    );
    $form['extra']['optional_properties'][] = $op_prop_fieldset;
    $op_prop++;
  }

  return $form;
}

/**
 * Implementation of _webform_render_component().
 */
function _webform_render_pure360($component, $value = NULL, $filter = TRUE) {

  $element['pure360'] = array(
    '#type' => 'hidden',
  );
  return $element;
}